let webLanguage = "TH";
let menu1;
let menu2;
let menu3;

this.setupLanguage(webLanguage);

// ========== [Function] ========== //
function setupLanguage(language) {
    if (language == "TH") {
        // === Navbar === //
        menu1 = "หน้าหลัก";
        menu2 = "พลังงานแสงอาทิตย์";
        menu3 = "แบตเตอรี่";
    }
    else {
        // === Navbar === //
        menu1 = "Home";
        menu2 = "Solar";
        menu3 = "Battery";
    }

    document.getElementById('navbarMenu1').innerHTML = menu1;
    document.getElementById('navbarMenu2').innerHTML = menu2;
    document.getElementById('navbarMenu3').innerHTML = menu3;
}

function onChangeLanguage(selectObject) {
    this.webLanguage = selectObject.value;
    this.setupLanguage(this.webLanguage);
}